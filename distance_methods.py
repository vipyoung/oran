__author__ = "Sofiane"

import geopy, geopy.distance
import math
import time
from GoogleTraffic import GoogleRouteInfo

def haversine(pt1, pt2):
	"""
	Calculate the great circle distance between two points
	on the earth (specified in decimal degrees)
	Sofiane: got it from:http://stackoverflow.com/questions/15736995/how-can-i-quickly-estimate-the-distance-between-two-latitude-longitude-points
	:param pt1: point (lat, lon)
	:param pt2: point (lat, lon)
	:return: the distance in meters
	"""

	lon1 = pt1[1]
	lat1 = pt1[0]
	lon2 = pt2[1]
	lat2 = pt2[0]

	# convert decimal degrees to radians
	lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])

	# haversine formula
	dlon = lon2 - lon1
	dlat = lat2 - lat1
	a = math.sin(dlat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
	c = 2 * math.asin(math.sqrt(a))
	km = 6367 * c
	return km * 1000


def geopy_distance(pt1, pt2):
	"""
	distance in meters between two points using geopy module.
	:param pt1: lat, lon
	:param pt2: lat, lon
	:return: distance in meters
	"""
	s = geopy.Point(pt1)
	t = geopy.Point(pt2)
	return geopy.distance.distance(s, t).meters



if __name__ == '__main__':
	# The points should be pairs of (latitude, longitude)
	# For information, latitude is the y-axis, longitude in the x-axis.
	p1 = (25.290559970000000, 51.500446029999999)
	p2 = (25.293479820000002, 51.483499410000000)

	print 'Haversine distance:', haversine(p1, p2)
	print 'Geopy distance:', geopy_distance(p1, p2)

	# Google Information
	# You need to get Google API Key: https://developers.google.com/maps/documentation/javascript/get-api-key#key
	api_key = 'yourkey'
	geo = GoogleRouteInfo(api_key)
	# Get the current time (as a timestamp)
	now_ts = int(time.time())
	# Get route information. Depart and arrive should be strings in the format: 'lat,lon'
	depart = '%s,%s' % p1
	arrive = '%s,%s' % p2
	direction = geo.get_direction(departure=depart, arrival=arrive, departure_time=now_ts)

	print 'driving distance (meters):', geo.get_gdistance(direction)
	print 'driving time (seconds):', geo.get_gduration_in_traffic(direction)