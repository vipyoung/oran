This repo is about different codes and functions I share with students, mainly from Oran University (Algeria)

## GoogleTraffic.py
This class contains some functions to request direction information from a "departure" address to an "arrival" address.
Addresses can be provided in different format. We'll use geo coordinates (latitude, longitude).
We can get in-traffic information if we provide a timestamp to the query. 

## distance_methods.py
In this script, we provide three methods:
### haversine:
This methods computes the distance in meters between two pairs of coordinates. 

### geopy_distance:
Similar to haversine, but it uses geopy distance method.

Notice that results of these two functions are not exactly the same. 
